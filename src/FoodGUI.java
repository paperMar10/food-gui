import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.HashMap;
import javax.swing.table.DefaultTableCellRenderer;


public class FoodGUI {

    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton yakisobaButton;
    private JButton ajihuraiButton;
    private JButton checkOutButton;
    private JTable orderedTable;
    private JLabel totalPrice;
    private JButton cancelOrderButton;
    private JPanel orderList;
    private JTabbedPane tabbedPane1;
    private JButton riceButton;
    private JButton misoSoupButton;
    private JButton setAButton;
    private JButton saladButton;
    private JButton setBButton;
    private JButton setCButton;
    private JButton softDrinkButton;
    private JButton alcoholButton;

    static class Food{
        final String foodName;
        final int  price;
        final String picturePath;

        Food(String foodName, int price, String picturePath){/*コンストラクタ*/
            this.foodName=foodName;
            this.price=price;
            this.picturePath=picturePath;

        }
    }

    /*option（サイズとか）ある場合*/
    static class OpFood extends Food{
        final String[] opNames;
        final int[] opPrices;
        final String whatSelect;
        OpFood(String foodName, int price, String picturePath, String[] opNames,int[] opPrices, String whatSelect){
            super(foodName,price,picturePath);
            this.opNames=opNames;
            this.opPrices=opPrices;
            this.whatSelect=whatSelect;
        }
    }

    int total = 0;  /*合計金額*/
    HashMap<String, Integer> orderedItems = new HashMap<String, Integer>(); /*注文した食べ物と個数*/

    DefaultTableModel orderedList = new DefaultTableModel(new String[]{"Item","Quantity","Subtotal"},0);/*注文リストデータ*/

    void order(String foodName,int price){/*注文確認・受付ダイアログ表示*/
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0){/*Yes*/
            JOptionPane.showMessageDialog(null,"Order for "+foodName+" received.");
            orderUpdate(foodName,price);
        }
    }

    void orderUpdate(String foodName,int price){/*合計金額，リスト更新*/
        if(!orderedItems.containsKey(foodName)){/*初回*/
            orderedItems.put(foodName,1);
            orderedList.addRow(new String[]{foodName, String.valueOf(1), String.valueOf(price)});
        }else{/*同じアイテム2個目以降*/
            orderedItems.replace(foodName,orderedItems.get(foodName)+1);
            orderedList.setValueAt(String.valueOf(
                    orderedItems.get(foodName)),
                    getRowByFoodName(orderedList, foodName),
                    1/*quantity*/);
            orderedList.setValueAt(String.valueOf(
                    orderedItems.get(foodName)*price),
                    getRowByFoodName(orderedList, foodName),
                    2/*Subtotal*/);
        }
        total+=price;
        totalPrice.setText("Total "+total+" yen");
    }

    int getRowByFoodName(DefaultTableModel orderedList, String foodName) {/*foodNameのある行番号を返す*/
        int i;
        for (i = 0; i <= orderedList.getRowCount(); i++) {
            if (orderedList.getValueAt(i, 0/*Item*/).equals(foodName))    break;
        }
        return i;
    }

    void totalReset(){  /*合計金額とリストをリセット*/
        total = 0;
        orderedItems.clear();
        orderedList.setRowCount(0);
        totalPrice.setText("Total "+total+" yen");
    }

    ImageIcon resize(ImageIcon imageIcon){/*画像の大きさを調整する*/
        Image image = imageIcon.getImage();
        Image resized = image.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
        return  new ImageIcon(resized);
    }

    String[] setButtonText(int basicPrice,String[] opNames,int[] opPrices){
        String[] buttonText=new String[opNames.length];
        for(int i=0; i<opNames.length; i++){
            int gap=opPrices[i]-basicPrice;
            if(gap==0){
                buttonText[i]=opNames[i];
            } else if (gap<0) {
                buttonText[i]="<html><center>"+opNames[i]+"<br />(" +gap+" yen)";
            }else{
                buttonText[i]="<html><center>"+opNames[i]+"<br />(+" +gap+" yen)";
            }
        }
        return buttonText;
    }

    void foodButton(JButton foodButton,Food food){
        foodButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(food.foodName,food.price);
            }
        });
        if (!"null".equals(food.picturePath)) {
            foodButton.setIcon(resize(new ImageIcon(this.getClass().getResource(food.picturePath))));
        }
        foodButton.setText("<html>"+food.foodName+"<br />"+food.price+" yen");
    }

    void opFoodButton(JButton opFoodButton,OpFood opFood){
        opFoodButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int select = JOptionPane.showOptionDialog(
                        null,
                        "Please select "+opFood.whatSelect.toLowerCase()+".",
                        opFood.whatSelect+" Select",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.INFORMATION_MESSAGE,
                        null,
                        setButtonText(opFood.price,opFood.opNames,opFood.opPrices),
                        0);

                if (select != JOptionPane.CLOSED_OPTION) {
                    order(opFood.foodName + "(" + opFood.opNames[select] + ")", opFood.opPrices[select]);
                }
            }
        });
        if (!"null".equals(opFood.picturePath)) {
            opFoodButton.setIcon(resize(new ImageIcon(this.getClass().getResource(opFood.picturePath))));
        }
        opFoodButton.setText("<html>"+opFood.foodName+"<br />"+opFood.price+" yen");
    }

    public FoodGUI() {
        /*注文リスト*/
        orderedTable.setModel(orderedList);
        orderedTable.setShowHorizontalLines(false);
        orderedTable.getColumnModel().getColumn(0/*Item*/).setPreferredWidth(150);
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
        orderedTable.getColumnModel().getColumn(1/*Quantity*/).setCellRenderer(cellRenderer);
        orderedTable.getColumnModel().getColumn(2/*Subtotal*/).setCellRenderer(cellRenderer);
        orderedTable.setEnabled(false);

        /*合計金額*/
        totalPrice.setText("Total "+total+" yen");

        /*Food Buttons*/
        Food tempura = new Food("Tempura",120,"pictures/tempuraPic.jpg");
        foodButton(tempuraButton,tempura);
//        tempuraButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                order(tempura.foodName,tempura.price);
//            }
//        });
//        tempuraButton.setIcon(resize(new ImageIcon(this.getClass().getResource(tempura.picturePath))));

        Food ramen = new Food("Ramen",130,"pictures/ramenPic.jpg");
        foodButton(ramenButton,ramen);

        Food udon = new Food("Udon",140,"pictures/udonPic.jpg");
        foodButton(udonButton,udon);

        Food karaage = new Food("Karaage",150,"pictures/karaagePic.jpg");
        foodButton(karaageButton,karaage);

        Food yakisoba = new Food("Yakisoba",160,"pictures/yakisobaPic.jpg");
        foodButton(yakisobaButton,yakisoba);

        Food ajihurai = new Food("Ajihurai",170,"pictures/ajihuraiPic.jpg");
        foodButton(ajihuraiButton,ajihurai);

        /*Checkout*/
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){/*Yes*/
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+total+" yen.");
                    totalReset();
                }
            }
        });

        /*Cancel Order*/
        cancelOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel order?",
                        "Cancel Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){/*Yes*/
                    JOptionPane.showMessageDialog(null,"The order has been canceled.");
                    totalReset();
                }
            }
        });


        String[] opSize=new String[]{"S","M","L"};
        int[] opRicePrices=new int[]{40,50,60};
        OpFood rice = new OpFood("Rice",50,"null",opSize,opRicePrices,"Size");
        opFoodButton(riceButton,rice);

        String[] opMisoSoupIngredients=new String[]{"tofu","pork"};
        int[] opMisoSoupPrices=new int[]{70,100};
        OpFood misoSoup = new OpFood("MisoSoup",70,"null",opMisoSoupIngredients,opMisoSoupPrices,"Ingredients");
        opFoodButton(misoSoupButton,misoSoup);

        String[] opSalad=new String[]{"Coleslaw","Macaroni","Fruit"};
        int[] opSaladPrices=new int[]{80,80,120};
        OpFood salad = new OpFood("Salad",80,"null",opSalad,opSaladPrices,"Salad");
        opFoodButton(saladButton,salad);

        Food setA = new Food("Set A",1000,"pictures/sake.jpg");
        foodButton(setAButton,setA);
        setAButton.setToolTipText("<html>Grilled salmon<br>Rice<br>Salad<br>Japanese pickles<br>mini-sizedUdon");

        Food setB = new Food("Set B",1200,"pictures/ebihurai.jpg");
        foodButton(setBButton,setB);
        setBButton.setToolTipText("<html>Deep Fried Shrimp<br>Rice<br>Salad<br>Miso Soup<br>Japanese pickles");

        Food setC = new Food("Set C",2000,"pictures/gyutan.jpg");
        foodButton(setCButton,setC);
        setCButton.setToolTipText("<html>Beef tongue<br>Rice<br>Ox tail soup");

        String[] opSoftDrink=new String[]{"Green Tea","Coffee","Orange Juice","Coke"};
        int[] opSoftDrinkPrices=new int[]{200,200,200,200};
        OpFood softDrink = new OpFood("Soft Drink",200,"pictures/360_F_65269782_Su4P08X48gcJkXLS5jTIUmYFvNjoy1oG.jpg",opSoftDrink,opSoftDrinkPrices,"Soft Drink");
        opFoodButton(softDrinkButton,softDrink);

        String[] opAlcohol=new String[]{"Beer","Whisky soda","Lemon sour"};
        int[] opAlcoholPrices=new int[]{300,300,300};
        OpFood alcohol = new OpFood("Alcohol",300,"pictures/adpDSC_3873-.jpg",opAlcohol,opAlcoholPrices,"Alcohol");
        opFoodButton(alcoholButton,alcohol);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
